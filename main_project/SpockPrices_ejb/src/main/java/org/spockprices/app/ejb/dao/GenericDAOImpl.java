package org.spockprices.app.ejb.dao;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.spockprices.app.entity.AbstractEntity;

public abstract class GenericDAOImpl<E extends AbstractEntity<P>, P> implements
		GenericDAO<E, P> {

	@PersistenceContext(unitName = "SpockPrices_jpa")
	protected EntityManager entityManager;

	public P insert(E entity) {
		entityManager.persist(entity);
		return (P) entity.getId();
	}

	public E find(P id) {
		return entityManager.find(getEntityClass(), id);
	}

	public void update(E entity) {
		entityManager.merge(entity);
	}

	public void delete(E entity) {
		entityManager.remove(entity);
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

	@SuppressWarnings("unchecked")
	public Class<E> getEntityClass() {
		if (entityClass == null) {
			Type type = getClass().getGenericSuperclass();
			
			if (type instanceof ParameterizedType) {
				ParameterizedType paramType = (ParameterizedType) type;

				entityClass = (Class<E>) paramType.getActualTypeArguments()[0];
			} else {
				throw new IllegalArgumentException(
						"Could not guess entity class by reflection");
			}
		}
		
		return entityClass;
	}

	private Class<E> entityClass;

}
