package org.spockprices.app.ejb.dao;

import javax.persistence.EntityManager;

import org.spockprices.app.entity.AbstractEntity;

public interface GenericDAO<E extends AbstractEntity<P>, P> {

	/**
	 * Persist the indicated entity to database
	 * 
	 * @param entity
	 * @return the primary key
	 */
	public P insert(E entity);

	/**
	 * Retrieve an object using indicated ID
	 * 
	 * @param id
	 * @return
	 */
	public E find(P id);

	/**
	 * Update indicated entity to database
	 * 
	 * @param entity
	 */
	public void update(E entity);

	/**
	 * Delete indicated entity from database
	 * 
	 * @param entity
	 */
	public void delete(E entity);

	/**
	 * Get the entity manager
	 * 
	 * @return
	 */
	public EntityManager getEntityManager();

	/**
	 * Return the entity class
	 * 
	 * @return
	 */
	public Class<E> getEntityClass();

}
