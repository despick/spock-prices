package org.spockprices.app.ejb.dao.person;

import javax.ejb.Local;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import org.spockprices.app.api.dao.IPerson;
import org.spockprices.app.api.dao.IPersonDao;
import org.spockprices.app.ejb.dao.GenericDAOImpl;
import org.spockprices.app.entity.person.Person;

@Stateless
@Local(IPersonDao.class)
@Remote(IPersonDao.class)
public class PersonDao extends GenericDAOImpl<Person, Integer> implements IPersonDao {
	
	public IPerson create(Integer id, String name) {
		Person p = new Person();
		p.setId(id);
		p.setName(name);
		return p;
	}

	@Override
	public Integer insert(IPerson entity) {
		getEntityManager().persist(entity);
		return entity.getId();
	}

	@Override
	public Person find(Integer id) {
		return getEntityManager().find(getEntityClass(), id);
	}

	@Override
	public void update(IPerson entity) {
		getEntityManager().merge(entity);

	}

	@Override
	public void delete(IPerson entity) {
		getEntityManager().remove(entity);

	}

}
