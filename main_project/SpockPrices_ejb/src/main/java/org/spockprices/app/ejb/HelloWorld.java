package org.spockprices.app.ejb;

import javax.ejb.Local;
import javax.ejb.Stateless;

import org.spockprices.app.api.HelloWorldLocal;

@Stateless
@Local(HelloWorldLocal.class)
public class HelloWorld implements HelloWorldLocal {

	@Override
	public String sayHello() {
		return "Hell World!";
	}

}
