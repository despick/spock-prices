package org.spockprices.app.ejb;

//import java.util.logging.Logger;
//
//import javax.annotation.PostConstruct;
//import javax.annotation.PreDestroy;
//import javax.annotation.Resource;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
//import javax.jms.Connection;
//import javax.jms.ConnectionFactory;
//import javax.jms.JMSException;
//import javax.jms.MessageProducer;
//import javax.jms.Queue;
//import javax.jms.Session;
//import javax.jms.TextMessage;

import org.spockprices.app.api.EmailQueueSenderLocal;

@Stateless
@Local(EmailQueueSenderLocal.class)
@Remote(EmailQueueSenderLocal.class)
public class EmailQueueSender implements EmailQueueSenderLocal {

//	private static Logger logger = Logger.getLogger(EmailQueueSender.class.toString());
//
//	@Resource(mappedName = "jms/ConnectionFactory")
//	private ConnectionFactory connectionFactory;
//
//	@Resource(mappedName = "jms/EmailQueue")
//	private Queue queue;
//
//	Connection connection;
//	Session session;
//	MessageProducer messageProducer;
//
//	@PostConstruct
//	public void init() {
//		try {
//			connection = connectionFactory.createConnection();
//			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
//			messageProducer = session.createProducer(queue);
//		} catch (JMSException e) {
//			logger.severe("JMS init failed: " + EmailQueueSender.class.toString());
//		}
//	}
//
	@Override
	public void sendEmailToQueue() {
//		if (messageProducer != null) {
//			try {
//			TextMessage message = session.createTextMessage();
//			message.setText("This is the email message");
//			
//			logger.info("Sending message: " + message.getText());
//			messageProducer.send(message);
//			} catch (JMSException e) {
//				logger.severe("JMS sending message failed: " + EmailQueueSender.class.toString());
//			}
//		}
	}
//	
//	@PreDestroy
//	public void destroy() {
//		try {
//			connection.close();
//		} catch (JMSException e) {
//			logger.severe("JMS close connection failed: " + EmailQueueSender.class.toString());
//		}
//	}

}
