package org.spockprices.app.entity;

public abstract class AbstractEntity<P> {

	public abstract P getId();
	
}
