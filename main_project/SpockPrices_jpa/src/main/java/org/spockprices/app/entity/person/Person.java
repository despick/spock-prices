package org.spockprices.app.entity.person;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.spockprices.app.api.dao.IPerson;
import org.spockprices.app.entity.AbstractEntity;

@Entity
@Table(name = "PERSONS", schema = "spockprices")
public class Person extends AbstractEntity<Integer> implements IPerson {

	private static final long serialVersionUID = 438274273832198991L;
	
	private static final String PERSON_GENERATOR = "PERSON_GENERATOR";
	private static final String SEQ_PERSON = "SEQ_PERSON";

	@Id
	@SequenceGenerator(name = PERSON_GENERATOR, sequenceName = SEQ_PERSON)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = PERSON_GENERATOR)
	private Integer id;

	private String name;

	public Person() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}