package org.spockprices.app.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/*")
public class SpockRESTApplication extends Application {

}
