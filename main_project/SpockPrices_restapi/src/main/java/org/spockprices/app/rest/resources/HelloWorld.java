package org.spockprices.app.rest.resources;

import javax.ws.rs.Consumes;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.spockprices.app.rest.entities.Hello;

@Path("/helloworld")
public class HelloWorld {
	
	@GET
	public String sayHello() {
		return "HelloWorld";
	}
	
	@Path("/xml")
	@POST
	@Produces(MediaType.APPLICATION_XML)
	@Consumes(MediaType.APPLICATION_XML)
	public Hello sayHelloXML(Hello hello) {
		return hello;
	}
	
	@Path("/json")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Hello sayHelloJSON(Hello hello) {
		return hello;
	}

}
