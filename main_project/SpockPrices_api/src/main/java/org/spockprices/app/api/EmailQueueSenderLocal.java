package org.spockprices.app.api;

public interface EmailQueueSenderLocal {

	public void sendEmailToQueue();
	
}
