package org.spockprices.app.api.dao;

public interface IPersonDao {
	
	public Integer insert(IPerson entity);
	
	public IPerson find(Integer id);
	
	public void update(IPerson entity);
	
	public void delete(IPerson entity);
	
	public IPerson create(Integer id, String name);
	
}
