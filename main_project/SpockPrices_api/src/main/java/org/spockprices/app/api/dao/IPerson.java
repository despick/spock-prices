package org.spockprices.app.api.dao;

import java.io.Serializable;

public interface IPerson extends Serializable {
	
	public Integer getId();

	public void setId(Integer id);

	public String getName();

	public void setName(String name);

}
