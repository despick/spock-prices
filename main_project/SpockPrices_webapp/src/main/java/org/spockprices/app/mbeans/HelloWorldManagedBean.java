package org.spockprices.app.mbeans;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class HelloWorldManagedBean implements Serializable {

	private static final long serialVersionUID = 887000498123573435L;

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHello() {
		String greeting = null;
		
		if (name != null && !name.isEmpty()) {
			greeting = "Hello " + name;
		}
		
		return greeting;
	}
}
