package org.spockprices.app.servlet;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.spockprices.app.api.EmailQueueSenderLocal;

@WebServlet("/SendEmail")
public class SendEmail extends HttpServlet {
	
	private static final long serialVersionUID = -4376851227083404919L;

	@EJB EmailQueueSenderLocal sendEmail;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		sendEmail.sendEmailToQueue();
		response.getOutputStream().print("Email Send?");
	}
	
}
