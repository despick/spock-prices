package org.spockprices.app.servlet;

import java.io.IOException;


import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.spockprices.app.api.dao.IPerson;
import org.spockprices.app.api.dao.IPersonDao;

@WebServlet("/AddPerson")
public class AddPerson extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	@EJB IPersonDao personManager;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		IPerson p = personManager.create(Integer.parseInt(request.getParameter("id")), request.getParameter("name"));
		personManager.insert(p);
	}

}
