package org.spockprices.app.servlet;

import java.io.IOException;


import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.spockprices.app.api.HelloWorldLocal;


@WebServlet("/HelloWorld")
public class HelloWorldServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	@EJB HelloWorldLocal helloWorld;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getOutputStream().print(helloWorld.sayHello());
	}

}
